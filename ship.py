class Ship():
    def __init__(self,shipid=0,amount=0):
        self.id = shipid
        self.amount = amount
        self.name = 'Undefined'
        #this is ugly, maybe could be improved
        if self.id is 202:
            self.name = 'Small cargo'
        elif self.id is 203:
            self.name = 'Large cargo'
        elif self.id is 204:
            self.name = 'Light Fighter'
        elif self.id is 205:
            self.name = 'Heavy Fighter'
        elif self.id is 206:
            self.name = 'Cruiser'
        elif self.id is 207:
            self.name = 'Battleship'
        elif self.id is 208:
            self.name = 'Colony ship'
        elif self.id is 209:
            self.name = 'Recycler'
        elif self.id is 210:
            self.name = 'Espionage probe'
        elif self.id is 211:
            self.name = 'Bomber'
        elif self.id is 213:
            self.name = 'Destroyer'
        elif self.id is 214:
            self.name = 'Deathstar'
        elif self.id is 215:
            self.name = 'Battlecruiser'
    def __str__(self):
        return (self.name+':'+str(self.amount))
