import sys
import os
import logging
from PySide2 import QtXml, QtGui, QtCore
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QPushButton, QLineEdit, QShortcut
from PySide2.QtGui import QKeySequence
from PySide2.QtCore import QFile, Slot
from botthread import BotThread
from config import options

class QSignaler(QtCore.QObject):
    log_message = QtCore.Signal(str)

class LoggerSignalHandler(logging.Handler):
    """Logging handler to emit QtSignal with log record text."""

    def __init__(self, *args, **kwargs):
        super(LoggerSignalHandler, self).__init__(*args, **kwargs)
        self.emitter = QSignaler()

    def emit(self, logRecord):
        msg = "{0}".format(logRecord.getMessage())
        self.emitter.log_message.emit(msg)
        # When the line below is enabled, logging is immediate/otherwise events
        # on the queue will be processed when the slot has finished.
        # QtGui.qApp.processEvents()

def spawn_bot():
    
    if ui.checkBox_rememberMe.isChecked():
        options.set('credentials','user', user.text())
        options.set('credentials', 'pass', password.text())
        options.set('credentials','server', server.currentText())
        options.set('general','remember_me','1')

    bot = BotThread(user.text(), password.text(),server.currentText())
    bot.start()

@Slot(bool)
def set_buildings_enabled(status):
    if status:
        options.set('building','enabled', '1')
    else:
        options.set('building','enabled', '0')

@Slot(bool)
def set_stations_enabled(status):
    if status:
        options.set('station','enabled','1')
    else:
        options.set('station','enabled','0')

@Slot(bool)
def set_researchs_enabled(status):
    if status:
        options.set('research','enabled','1')
    else:
        options.set('research','enabled','0')

@Slot(bool)
def set_fleet_enabled(status):
    if status:
        options.set('fleet','enabled','1')
    else:
        options.set('fleet','enabled','0')

@Slot(str)
def set_fleet_resource_priority(priority):
    print(priority)
    options.set('fleet','resources_priority',priority)

@Slot(str)
def set_fleet_speed(speed):
    options.set('fleet','speed',speed)

@Slot(str)
def set_fleet_save_time(time):
    if time == '':
        time = '0'
    options.set('fleet','save_time',time)

@Slot(str)
def set_fleet_save_random(time):
    if time == '':
        time = '0'
    options.set('fleet','save_random',time)


if __name__ == "__main__":

    if getattr(sys, 'frozen', False):
        application_path = sys._MEIPASS + "\\"
    else:
        application_path = os.path.dirname(os.path.abspath(__file__)) + '/'

    logger_signal_handler = LoggerSignalHandler()

    #get the logger instance
    logger = logging.getLogger("selenium")
    logger.addHandler(logger_signal_handler)
    
    app = QApplication(sys.argv)

    file = QFile(application_path + 'main.ui')
    file.open(QFile.ReadOnly)

    loader = QUiLoader()
    ui = loader.load(file)

    startButton = ui.pushButton_start
    startButton.clicked.connect(spawn_bot)
    startButton.setShortcut(QKeySequence('Return'))

    user = ui.lineEdit_user
    password = ui.lineEdit_pass
    server = ui.comboBox_server
    guilog = ui.plainTextEdit_logOutput

    buildingsEnable = ui.checkBox_buildingsEnable
    stationsEnable = ui.checkBox_stationsEnable
    researchEnable = ui.checkBox_researchEnable
    fleetSavingEnable = ui.checkBox_fleetSavingEnable    
    resourcePriority = ui.comboBox_resourcePriority
    fleetSaveSpeed = ui.comboBox_fleetSaveSpeed
    fleetSaveTime = ui.lineEdit_fleetSaveTime
    fleetSaveRandomizer = ui.lineEdit_fleetSaveRandomizer

    #connects the signal of the loggerHandler to the TexEdit
    logger_signal_handler.emitter.log_message.connect(guilog.appendPlainText)

    #fill the gui with the config data    
    if options['general']['remember_me'] == '0':
        ui.checkBox_rememberMe.setChecked(False)
    else:
        ui.checkBox_rememberMe.setChecked(True)
        user.setText(options['credentials']['user'])
        password.setText(options['credentials']['pass'])
        index = server.findText(options['credentials']['server'])
        if index != -1:
            server.setCurrentIndex(index)

    if options['building']['enabled'] == '1':
        buildingsEnable.setChecked(True)
    else:
        buildingsEnable.setChecked(False)
    
    if options['station']['enabled'] == '1':
        stationsEnable.setChecked(True)
    else:
        stationsEnable.setChecked(False)

    if options['research']['enabled'] == '1':
        researchEnable.setChecked(True)
    else:
        researchEnable.setChecked(False)
    
    if options['fleet']['enabled'] == '1':
        fleetSavingEnable.setChecked(True)
    else:
        fleetSavingEnable.setChecked(False)
    resourcePriority.setCurrentIndex(resourcePriority.findText(options['fleet']['resources_priority']))
    fleetSaveSpeed.setCurrentIndex(fleetSaveSpeed.findText(options['fleet']['speed']))
    fleetSaveTime.setText(options['fleet']['save_time'])
    fleetSaveRandomizer.setText(options['fleet']['save_random'])

    #connects gui change signals to handlers, to update the config.ini 
    buildingsEnable.stateChanged.connect(set_buildings_enabled)
    stationsEnable.stateChanged.connect(set_stations_enabled)
    researchEnable.stateChanged.connect(set_researchs_enabled)
    fleetSavingEnable.stateChanged.connect(set_fleet_enabled)
    resourcePriority.currentTextChanged.connect(set_fleet_resource_priority)
    fleetSaveSpeed.currentTextChanged.connect(set_fleet_speed)
    fleetSaveTime.textEdited.connect(set_fleet_save_time)
    fleetSaveRandomizer.textEdited.connect(set_fleet_save_random)    

    ui.show()

    sys.exit(app.exec_())
